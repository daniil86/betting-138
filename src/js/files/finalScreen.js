export function showFinalScreen(score, status = 'lose') {
	const final = document.querySelector('.final');
	const finalScore = document.querySelector('.final ._win-score');
	const finalTitle = document.querySelector('.final__title');

	if (status === 'win') {
		finalScore.textContent = `+${score}`;
		final.classList.add('_win');
		finalTitle.textContent = 'YOU win';
	} else if (status === 'lose') {
		final.classList.add('_lose');
		finalTitle.textContent = 'YOU lose';
		finalScore.textContent = `+${score}`;
	}

	setTimeout(() => {
		final.classList.add('_visible');
	}, 50);
}

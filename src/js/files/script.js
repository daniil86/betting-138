
import { getRandom, getDigFormat, addMoney, addResource, deleteMoney, getRandomNumArr, shuffle } from '../files/functions.js';
import { startData } from './startData.js';
import { showFinalScreen } from './finalScreen.js';


export function initStartData() {

	if (!localStorage.getItem('money')) {
		localStorage.setItem('money', startData.bank);
	}
	writeScore();

	// if (!localStorage.getItem('resource')) {
	// 	localStorage.setItem('resource', 0);
	// }
	// writeResource();


	if (!localStorage.getItem('current-bet')) {
		localStorage.setItem('current-bet', startData.countBet);
	}
	// writeBet();


	if (document.querySelector('.main__hero span')) document.querySelector('.main__hero span').textContent = localStorage.getItem('user-name');

	// if (!localStorage.getItem('level')) {
	// 	localStorage.setItem('level', 1);
	// }

}


function writeScore() {
	if (document.querySelector('.score')) {
		let money = getDigFormat(+localStorage.getItem('money'));
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = money;
		})
	}
}

// function writeResource() {
// 	if (document.querySelector('.resource')) {
// 		let money = getDigFormat(+localStorage.getItem('resource'));
// 		document.querySelectorAll('.resource').forEach(el => {
// 			el.textContent = money;
// 		})
// 	}
// }

export function writeBet() {
	if (document.querySelector(startData.nameItemBet)) {
		document.querySelectorAll(startData.nameItemBet).forEach(el => {
			el.textContent = localStorage.getItem('current-bet');
		})
	}
}



initStartData();

//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-game');

function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (anim_items.length) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}


//========================================================================================================================================================
// game

const timeBlock = document.querySelector('.game__timer span');
const gameBalls = document.querySelector('.game__balls');
const userAim = document.querySelector('[data-aim="user"]');
const botAim = document.querySelector('[data-aim="bot"]');
const buster_1 = document.querySelector('[data-buster="1"]');
const buster_2 = document.querySelector('[data-buster="2"]');

const userScore = document.querySelectorAll('[data-count="1"]');
const botScore = document.querySelectorAll('[data-count="2"]');

export const configGame = {
	state: 1, // 1 - no start game, 2 - playing game
	currentLevel: null,

	timeConst: 60, // 60
	timeCurrent: 0,
	timeSecLimit: 0,
	timer: false,

	lastTime: 0,

	balls: [],
	maxBalls: 5,
	idx: 0,

	minVelocity: 2,
	maxVelocity: 20,//2

	timeCreateBall: 0,
	timeLimitCreateBall: 1500,

	winCountMoney: 25,

	userScore: 0,
	botScore: 0,

	winCount: 0,

	busters: {
		isBonus_1_Buying: true,
		isBonus_2_Buying: true,
		buster_1: {
			isActive: false,
			constTime: 5000,
			currentTime: 0
		},
		buster_2: {
			isActive: false,
		},
	},
	bot: {
		isActive: false,
		constTime: 15000,
		currentTime: 0,
		constPeriodUsing: 1500,
		timeUsingPeriod: 0,
		width: botAim ? botAim.clientWidth : false,
		height: botAim ? botAim.clientHeight : false,
	},

	touch: {
		x: 0,
		y: 0
	},
	aim: {
		width: userAim ? userAim.clientWidth : false,
		height: userAim ? userAim.clientHeight : false,
	}
}

export function shoot(targetElement) {
	if (!targetElement.closest('.game__footer')) {
		moveAim();
		animateShootAim();
	}

	if (targetElement.closest('.bird')) {
		const idx = +targetElement.closest('.bird').dataset.idx;
		configGame.balls.forEach(ball => {
			if (ball.idx === idx) {
				ball.destroy('user');
			}
		});
	}
}

function moveAim() {
	userAim.style.top = `${configGame.touch.y - configGame.aim.height * 0.5}px`;
	userAim.style.left = `${configGame.touch.x - configGame.aim.width * 0.5}px`;
}
function animateShootAim() {
	setTimeout(() => {
		userAim.classList.add('_shoot');
	}, 100);
	setTimeout(() => {
		userAim.classList.remove('_shoot');
	}, 500);
}

//==============
// busters
export function useBuster_1() {
	configGame.busters.buster_1.isActive = true;
	configGame.busters.isBonus_1_Buying = false;
	updateBusters();
}

export function useBuster_2() {
	configGame.busters.buster_2.isActive = true;
	configGame.busters.isBonus_2_Buying = false;

	destroyAllActiveDucks();

	updateBusters();
}

function destroyAllActiveDucks() {
	const balls = document.querySelectorAll('.bird');

	for (let i = 0; i < configGame.balls.length; i++) {
		if (configGame.balls[i].inGame) {

			for (let k = 0; k < balls.length; k++) {
				if (balls[k].getAttribute('data-idx') == configGame.balls[i].idx && Math.abs(configGame.balls[i].y) > 200) {

					const idx = +balls[k].dataset.idx;
					configGame.balls.forEach(ball => {
						if (ball.idx === idx) {
							ball.destroy('user');
						}
					});
					break;
				}
			}
		}
	}
}


//==============

class Lighting {
	constructor(item, idx) {

		this.yOffset = 100;
		this.number = item;

		this.drawSizes(this.number);

		this.drawStartPosition(this.number);
		this.y = 0;

		this.item = document.querySelector(`[data-mountain="${this.number}"]`);

		this.idx = idx;

		this.inGame = false;

		this.vy = 1;

		this.ballClassName = 'bird';

		this.isDestroyed = false;

		this.draw();
	}

	drawSizes(item) {
		if (item === 1) {
			this.width = 52;
			this.height = 42;
		} else if (item === 2 || item === 3) {
			this.width = 129;
			this.height = 116;
		} else if (item === 4) {
			this.width = 173;
			this.height = 143;
		}
	}
	drawStartPosition(item) {
		if (item === 1 || item === 4) {
			this.x = getRandom(0, innerWidth * 0.5);
		} else if (item === 2) this.x = getRandom(0, innerWidth * 0.2);
		else if (item === 3) this.x = getRandom(innerWidth * 0.4, innerWidth * 0.8);
	}


	draw() {
		this.ball = document.createElement('div');
		this.ball.classList.add(this.ballClassName);

		this.ball.setAttribute('data-bird', getRandom(1, 5));

		this.ball.setAttribute('data-idx', this.idx);
		this.ball.style.cssText = `
			width: ${this.width}px;
			height: ${this.height}px;
		`


		this.item.append(this.ball);
	}

	update() {
		if (this.inGame && !this.isDestroyed && !configGame.busters.buster_1.isActive) {

			this.y -= this.vy;

			this.ball.style.transform = `translate(${this.x}px, ${this.y}px)`;

			if (Math.abs(this.y) > innerHeight + this.height + 50) {
				this.reset();
			}
		}
	}

	destroy(player) {
		this.isDestroyed = true;
		this.ball.style.transform = `translate(${this.x}px, ${this.y}px) rotate(${getRandom(-270, 270)}deg) scale(1.3)`;
		this.ball.style.transition = `all 0.7s cubic-bezier(0.175, 0.885, 0.32, 1.275) 0s`;
		this.ball.style.opacity = `0`;

		setTimeout(() => {
			this.reset();
			addMoney(configGame.winCountMoney, '.score', 500, 1500);

			if (player == 'user') {
				configGame.userScore++;
				configGame.winCount += 0.2 * +localStorage.getItem('current-bet');
			}
			else configGame.botScore++;

			drawScores();
		}, 700);
	}


	reset() {
		this.inGame = false;
		this.isDestroyed = false;
		// Определяем стартовые позиции мяча
		this.drawStartPosition(this.number);
		this.drawSizes(this.number);
		this.y = 0;

		this.updateVelocity();
		let rBird = getRandom(1, 5);

		this.ball.setAttribute('data-bird', rBird);

		this.ball.style.transform = `translate(${this.x}px, ${this.y}px)`;
		this.ball.style.opacity = `1`;
		this.ball.style.transition = `none`;

		this.ball.classList.remove('_active');

	}

	updateVelocity() {
		this.vy = getRandom(configGame.minVelocity, configGame.maxVelocity) / 10;
	}

	// Технический метод - для проверки где начало координат созданного экхемпляра
	createDot(x, y) {
		let dot = document.createElement('div');
		dot.classList.add('_dot');
		document.querySelector('.game').append(dot);
		dot.style.top = `${y}px`;
		dot.style.left = `${x}px`;
	}
}

if (document.querySelector('.game')) {
	initGame();
}

function initGame() {
	createBLightings();
}

// Создаем мячи со стартовыми позициями
function createBLightings() {
	// В каждую гору записываем по 5 птичек
	configGame.balls.push(new Lighting(1, 1));
	configGame.balls.push(new Lighting(2, 2));
	configGame.balls.push(new Lighting(3, 3));
	configGame.balls.push(new Lighting(4, 4));
	configGame.balls.push(new Lighting(1, 5));
	configGame.balls.push(new Lighting(2, 6));
	configGame.balls.push(new Lighting(3, 7));
	configGame.balls.push(new Lighting(4, 8));
	configGame.balls.push(new Lighting(1, 9));
	configGame.balls.push(new Lighting(2, 10));
	configGame.balls.push(new Lighting(3, 11));
	configGame.balls.push(new Lighting(4, 12));
	configGame.balls.push(new Lighting(1, 13));
	configGame.balls.push(new Lighting(2, 14));
	configGame.balls.push(new Lighting(3, 15));
	configGame.balls.push(new Lighting(4, 16));
	configGame.balls.push(new Lighting(1, 17));
	configGame.balls.push(new Lighting(2, 18));
	configGame.balls.push(new Lighting(3, 19));
	configGame.balls.push(new Lighting(4, 20));
}

export function addFreeBall(deltaTime) {
	if (configGame.timeCreateBall > configGame.timeLimitCreateBall) {
		const balls = document.querySelectorAll('.bird');

		for (let i = 0; i < configGame.balls.length; i++) {
			if (configGame.balls[i].inGame) continue;

			else if (!configGame.balls[i].inGame) {
				configGame.balls[i].inGame = true;

				balls.forEach(ball => {
					if (ball.getAttribute('data-idx') == configGame.balls[i].idx) {
						ball.classList.add('_active');
					}
				})
			}
			break;
		}
		configGame.timeCreateBall = 0;
	} else {
		configGame.timeCreateBall += deltaTime;
	}
}

// Перед переходом на сцену игры - проверяем какие куплены бустеры
export function checkActiveBusters() {
	configGame.busters.isBonus_1_Buying = true;
	configGame.busters.isBonus_2_Buying = true;

	updateBusters();
}

function updateBusters() {
	if (configGame.busters.isBonus_1_Buying) buster_1.textContent = 1;
	else {
		buster_1.textContent = 0;
		buster_1.classList.add('_hold');
	}

	if (configGame.busters.isBonus_2_Buying) buster_2.textContent = 1;
	else {
		buster_2.textContent = 0;
		buster_2.classList.add('_hold');
	}
}


export function startGame() {

	// После загрузки игры обновляем скорость перемещения в соответствии с уровнем
	configGame.balls.forEach(ball => {
		ball.updateVelocity();
	})

	configGame.state = 2;

	configGame.timeCurrent = configGame.timeConst;

	animateGame(0);
}

export function stopGame() {
	// Останавливаем анимацию
	configGame.state = 1;

	// Сбрасываем мячи на стартовые позиции
	configGame.balls.forEach(ball => ball.reset());

	configGame.winCount = 0;

	configGame.userScore = 0;
	configGame.botScore = 0;

	drawScores();
}

function drawScores() {
	userScore.forEach(item => item.textContent = configGame.userScore);
	botScore.forEach(item => item.textContent = configGame.botScore);
}

function timer(deltatime) {

	if (configGame.timeSecLimit >= 1000) {
		configGame.timeSecLimit = 0;

		--configGame.timeCurrent;

		timeBlock.textContent = `${configGame.timeCurrent}`;

		// Если время закончилось
		if (configGame.timeCurrent <= 0) {
			let isWin = configGame.userScore >= configGame.botScore ? 'win' : 'lose';
			showFinalScreen(configGame.winCount, isWin);
			addMoney(configGame.winCount, '.score', 1000, 2000);

			// Останавливаем анимацию
			configGame.state = 1;
		}
	} else {
		configGame.timeSecLimit += deltatime;
	}
}

function enemyFire(deltatime) {
	if (configGame.bot.timeUsingPeriod > configGame.bot.constPeriodUsing) {
		const balls = document.querySelectorAll('.bird');
		configGame.bot.timeUsingPeriod = 0;

		for (let i = 0; i < configGame.balls.length; i++) {
			if (configGame.balls[i].inGame) {

				for (let k = 0; k < balls.length; k++) {
					if (balls[k].getAttribute('data-idx') == configGame.balls[i].idx && Math.abs(configGame.balls[i].y) > 200) {
						shootBot(balls[k]);
						break;
					}
				}
				break;
			}
		}

	} else {
		configGame.bot.timeUsingPeriod += deltatime;
	}
}

export function shootBot(elem) {

	const xCoord = elem.getBoundingClientRect().left;
	const yCoord = elem.getBoundingClientRect().top;


	moveAimBot(xCoord, yCoord);
	animateShootAimBot();

	const idx = +elem.dataset.idx;
	configGame.balls.forEach(ball => {
		if (ball.idx === idx) {
			ball.destroy('bot');
		}
	});
}

function moveAimBot(x, y) {
	botAim.style.top = `${y - configGame.bot.height * 0.5}px`;
	botAim.style.left = `${x - configGame.bot.width * 0.5}px`;
}

function animateShootAimBot() {
	setTimeout(() => {
		botAim.classList.add('_shoot');
	}, 100);
	setTimeout(() => {
		botAim.classList.remove('_shoot');
	}, 500);
}

function animateGame(time) {
	const deltatime = time - configGame.lastTime;
	configGame.lastTime = time;

	// Таймер обратного отсчета - отображаем оставшееся время и проверяем закончилось ли время
	timer(deltatime);

	// Если не использован бонус заморозки времени - добавляем с интервалом новый мяч.
	// Если бонус использован - останавливаем движение и добавление новых мячей
	if (!configGame.busters.buster_1.isActive) addFreeBall(deltatime);
	else {
		configGame.busters.buster_1.currentTime += deltatime;
		if (configGame.busters.buster_1.currentTime >= configGame.busters.buster_1.constTime) {
			configGame.busters.buster_1.isActive = false;
			configGame.busters.buster_1.currentTime = 0;
		}
	}

	// Автоматически стреляет бот
	configGame.bot.currentTime += deltatime;
	if (configGame.bot.currentTime < configGame.bot.constTime) {
		enemyFire(deltatime);
	} else {
		configGame.bot.isActive = false;
		configGame.bot.currentTime = 0;
	}

	configGame.balls.forEach(ball => {
		if (ball.inGame) {
			ball.update();
		}
	});

	if (configGame.state === 2) requestAnimationFrame(animateGame);
}


//========================================================================================================================================================

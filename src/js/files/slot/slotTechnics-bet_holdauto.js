// Стандартная с одной кнопко запуска


export const configSlot = {
	currentWin: 0,
	winCoeff_1: 30,
	winCoeff_2: 30,

	rows_slot1: 3, // количество рядов с картинками (как правило всегда 3)
	rows_slot2: 3,

	minBet: 50,
	maxBet: 950,

	betSlot3: 50,

	isAutMode: false,
	isWin: false,

	timer: false
}

const configGSAP = {
	duration_1: 1,
	duration_3: 3
}



//========================================================================================================================================================
let slot1 = null;

class Slot1 {
	constructor(domElement, config = {}) {
		this.autospinActive = false;
		Symbol1.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "8", "1"]
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["4", "5", "6"],
			["7", "8", "1"]
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel1")).map(
			(reelContainer, idx) =>
				new Reel1(reelContainer, idx, this.currentSymbols[idx])
		);

		this.holder = null;
		this.spinButton = document.querySelector('[data-btn-slot="1"]');

		this.spinButton.addEventListener("touchstart", () => {
			let oThis = this;
			this.holder = setTimeout(function () {
				oThis.autospinActive = true;
				oThis.autoSpin();
			}, 2000)
		});
		this.spinButton.addEventListener("touchend", () => {

			this.holder && clearTimeout(this.holder);
			if (!this.autospinActive) {
				tl.to(this.spinButton, {});
				if ((+sessionStorage.getItem('money') >= configSlot.bet)) {
					this.spin();
					if (configSlot.isWin) {
						configSlot.isWin = false;
						checkWIn(false);
					}
				} else {
					noMoney('.score');
				}
			}
		});

		this.maxBetButton = document.querySelector('[data-btn-max="1"]');
		this.maxBetButton.addEventListener('click', () => {
			//при запуске сбрасываем интервал запуска между слотами
			tl.to(this.spinButton, {});

			const money = +sessionStorage.getItem('money');
			if (money > 1000) {
				configSlot.bet = 1000;
				this.spin();
			}
			else if (money < 1000 && money > 150) {
				const num = money - (money % 5);
				configSlot.bet = num - 100;

				this.spin();
			} else if (money < 150) noMoney('.score');
			configSlot.bet = configSlot.constBet;
		})

		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	autoSpin() {
		var oThis = this;
		this.casinoAutoSpinCount = 0;
		if ((+sessionStorage.getItem('money') > configSlot.bet)) {
			this.casinoAutoSpinCount++;
			this.spin();
		}
		let casinoAutoSpin = setInterval(function () {
			oThis.casinoAutoSpinCount++;
			if (oThis.casinoAutoSpinCount >= 9) oThis.autospinActive = false;

			if (configSlot.isWin) {
				configSlot.isWin = false;
				checkWIn(false);
			}

			tl.to(oThis.spinButton, {});

			if (oThis.casinoAutoSpinCount < 10 && (+sessionStorage.getItem('money') >= configSlot.bet)) {
				oThis.spin();
			} else if (oThis.casinoAutoSpinCount >= 10 && (+sessionStorage.getItem('money') >= configSlot.bet)) {
				clearInterval(casinoAutoSpin);
			} else if ((+sessionStorage.getItem('money') < configSlot.bet)) {
				clearInterval(casinoAutoSpin);
				noMoney('.score');
			}
		}, 5500);
	}

	async spin() {
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()]
		];

		this.onSpinStart(this.nextSymbols);

		await Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin(this.nextSymbols);
			})
		);
	}

	onSpinStart(symbols) {
		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');
		this.maxBetButton.classList.add('_hold');

		if (symbols)
			this.config.onSpinStart(symbols);
	}

	onSpinEnd(symbols) {
		if (!this.autospinActive) {
			this.spinButton.classList.remove('_hold');
			this.maxBetButton.classList.remove('_hold');
		}

		if (symbols) {
			this.config.onSpinEnd(symbols);
		}

		upProgress();
		openDisabledGame();
	}
}

class Reel1 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol1(symbol).img)
		);
	}

	get factor() {
		return 3 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol1(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	async spin(symbols) {
		// запускаем анимацию смещения колонки
		this.param = ((Math.floor(this.factor) * 10) / (3 + Math.floor(this.factor) * 10)) * 100;

		await tl.fromTo(this.symbolContainer, { translateY: 0, }, {
			translateY: `${-this.param}%`,
			duration: configGSAP.duration_1,
			onComplete: () => {

				// определяем какое количество картинок хотим оставить в колонке
				const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

				gsap.to(this.symbolContainer, { translateY: 0, duration: 0 });

				// запускаем цикл, в котором оставляем определенное количество картинок в конце колонки
				for (let i = 0; i < max; i++) {
					this.symbolContainer.firstChild.remove();
				}
			}
		}, '<10%');

		// После выполнения анимации запускаем сценарий разблокировки кнопок и проверки результата
		slot1.onSpinEnd(symbols);
	}
}

const cache1 = {};

class Symbol1 {
	constructor(name = Symbol1.random()) {
		this.name = name;

		if (cache1[name]) {
			this.img = cache1[name].cloneNode();
		} else {

			this.img = new Image();

			this.img.src = `img/game-1/slot-${name}.png`;

			cache1[name] = this.img;
		}
	}

	static preload() {
		Symbol1.symbols.forEach((symbol) => new Symbol1(symbol));
	}

	static get symbols() {
		return configSlot.arr_1;
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config1 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2]
		) {

			let currintWin = configSlot.bet * configSlot.winCoeff_1;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);

		}
	},
};

if (document.querySelector('#slot1')) {
	slot1 = new Slot1(document.getElementById("slot1"), config1);
}


//========================================================================================================================================================
if (targetElement.closest('[data-button="slot-1-home"]')) {
	wrapper.classList.add('_slot-screen');
	wrapper.classList.remove('_slot-1');
	if (configSlot.isAutMode) {
		clearInterval(configSlot.timer);
		configSlot.isAutMode = false;

		document.querySelector('[data-button="spin-1"]').classList.remove('_hold');
		document.querySelector('[data-button="auto-1"]').classList.remove('_hold');
	}
}
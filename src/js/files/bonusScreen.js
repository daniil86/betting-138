import { } from "./script.js";


const gameBonus = document.querySelector('.game__bonus-game');

export function openBonusChest(item) {
	item.classList.add('_visible');
	document.querySelector('.bonus-screen__boxes').classList.add('_hold');
	configGame.isKeyopen = false;
	gameBonus.classList.add('_disabled');

	const count = +item.dataset.bonus;
	if (count === 1) addMoney(500, '.score', 500, 1500);
	else if (count === 2) {
		configGame.isKeyopen = true;
		checkOpenedKey();
	}

	setTimeout(() => {
		document.querySelector('.bonus-screen').classList.remove('_visible');
		document.querySelector('.bonus-screen__boxes').classList.remove('_hold');
		exitBonusScreen();
	}, 1500);
}

function checkOpenedKey() {
	if (configGame.isKeyopen && gameBonus.classList.contains('_disabled')) {
		gameBonus.classList.remove('_disabled');
	}
}

function exitBonusScreen() {
	document.querySelectorAll('.bonus-screen__box').forEach(item => item.classList.contains('_visible') ? item.classList.remove('_visible') : false);
}

document.addEventListener('click', (e) => {

	const targetElement = e.target;

	if (targetElement.closest('.bonus-screen__button')) {
		document.querySelector('.bonus-screen').classList.remove('_visible');
		document.querySelector('.bonus-screen__boxes').classList.remove('_hold');
	}

	if (targetElement.closest('.bonus-screen__box')) {
		openBonusChest(targetElement.closest('.bonus-screen__box'));
	}

	if (targetElement.closest('.game__bonus-game')) {
		generateDataBonusGame();
		document.querySelector('.bonus-screen').classList.add('_visible');
	}

})
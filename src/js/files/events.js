
import { addMoney, deleteMoney, noMoney, addRemoveClass } from './functions.js';
import { checkActiveBusters, configGame, startGame, stopGame, shoot, useBuster_1, useBuster_2 } from './script.js';
import { startData } from './startData.js';


// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {

	configGame.touch.x = e.clientX;
	configGame.touch.y = e.clientY;

	const wrapper = document.querySelector('.wrapper');

	const targetElement = e.target;

	const money = +localStorage.getItem('money');
	const bet = +localStorage.getItem('current-bet');

	// privacy screen
	if (targetElement.closest('.preloader__button')) {
		location.href = 'main.html';
	}

	// main screen

	if (targetElement.closest('[data-button="privacy"]')) {
		location.href = 'index.html';
	}

	if (targetElement.closest('[data-button="levels"]')) {
		wrapper.classList.add('_levels');
	}
	if (targetElement.closest('[data-button="levels-home"]')) {
		wrapper.classList.remove('_levels');
	}

	if (targetElement.closest('[data-button="game"]')) {
		if (money > bet) {
			wrapper.classList.add('_game');

			checkActiveBusters();
			setTimeout(() => {
				startGame();
				deleteMoney(bet, '.score');
				if (localStorage.getItem('current-bet') == 300) {
					document.querySelector('.footer__level span').textContent = 'EASE';
				} else if (localStorage.getItem('current-bet') == 400) {
					document.querySelector('.footer__level span').textContent = 'MEDIUM';
				} else if (localStorage.getItem('current-bet') == 500) {
					document.querySelector('.footer__level span').textContent = 'HARD';
				}
			}, 250);
		} else noMoney('.score');

	}
	if (targetElement.closest('[data-button="game-home"]')) {
		wrapper.classList.remove('_game');
		setTimeout(() => {
			stopGame();
		}, 500);
	}

	if (targetElement.closest('[data-bet')) {
		const bet = +targetElement.closest('[data-bet').dataset.bet;
		localStorage.setItem('current-bet', bet);
	}

	if (targetElement.closest('.game')) {
		shoot(targetElement);
	}

	if (targetElement.closest('[data-button-buster]')) {
		const buster = +targetElement.closest('[data-button-buster]').dataset.buttonBuster;

		if (buster === 1) useBuster_1();
		else if (buster === 2) useBuster_2();
	}

	if (targetElement.closest('[data-button="final-menu"]')) {
		document.querySelector('.final').classList.remove('_visible');
		wrapper.classList.remove('_game');
		setTimeout(() => {
			stopGame();
		}, 500);
	}

	if (targetElement.closest('[data-button="final-reload"]')) {

		stopGame();
		checkActiveBusters();
		setTimeout(() => {
			const bet = +localStorage.getItem('current-bet');
			startGame();
			deleteMoney(bet, '.score');
		}, 250);
		setTimeout(() => {
			document.querySelector('.final').classList.remove('_visible');
		}, 500);
	}

})






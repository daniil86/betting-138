const startData = {
	bank: 6000, // 1500

	countBet: 300,
	maxBet: 1000,

	nameItemScore: '._score',
	nameItemBet: '._bet',

	store: {
		countBuying: 0,
		health: 2,
		speed: 2,
		capacity: 1
	},

	prices: {
		price_1: 35,
		price_2: 55,
		price_3: 75,
		price_4: 95,
		price_5: 15,
		price_6: 500,
		price_7: 500,
	}

}

export { startData }